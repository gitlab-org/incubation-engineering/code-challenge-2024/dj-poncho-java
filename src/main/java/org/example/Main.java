package org.example;

import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.api.metrics.Meter;
import io.opentelemetry.exporter.logging.LoggingMetricExporter;
import io.opentelemetry.api.metrics.LongCounter;
import io.opentelemetry.sdk.OpenTelemetrySdk;
import io.opentelemetry.sdk.metrics.SdkMeterProvider;
import io.opentelemetry.sdk.metrics.export.PeriodicMetricReader;
import io.opentelemetry.sdk.autoconfigure.AutoConfiguredOpenTelemetrySdk;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {

    private static final Logger logger = LogManager.getLogger();
    public static void main(String[] args) {

//        SdkMeterProvider meterProvider = SdkMeterProvider.builder()
//                .registerMetricReader(PeriodicMetricReader.builder(LoggingMetricExporter.create()).build())
//                .build();
//
//        OpenTelemetry openTelemetry = OpenTelemetrySdk.builder()
//                .setMeterProvider(meterProvider)
//                .buildAndRegisterGlobal();

        OpenTelemetry openTelemetry = AutoConfiguredOpenTelemetrySdk.initialize().getOpenTelemetrySdk();

        Meter meter = openTelemetry.getMeter(
                "com.example.helloworld");

        LongCounter errorCounter = meter.counterBuilder("errors_counter")
                .setDescription("Counts how many times the hello world has been updated")
                .setUnit("1")
                .build();

        logger.info("This is an information message");
        String log4jVersion = org.apache.logging.log4j.util.PropertiesUtil.class.getPackage().getImplementationVersion();
        if (!log4jVersion.equals("2.1")) {
            errorCounter.add(1);
        }

        try {
            Thread.sleep(Long.MAX_VALUE);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}